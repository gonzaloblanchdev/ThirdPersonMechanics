using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThirdPersonMovement : MonoBehaviour {

    [Header("References")]
    public CharacterController controller;
    public Transform camTransform;
    public Animator animator;
    [Header("Config")]
    public float speed = 6f;
    public float turnSmoothTime = 0.1f;

    // PRIVATES
    private float turnSmoothVelocity;
    
    void Update() {
        // Inputs
        float horizontal = Input.GetAxisRaw("Horizontal");
        float vertical = Input.GetAxisRaw("Vertical");
        // Direction
        Vector3 direction = new Vector3(horizontal, 0f, vertical).normalized;
        animator.SetBool("isMoving", (horizontal != 0 || vertical != 0));

        // If there's movement...
        if(direction.magnitude >= 0.1f) {
            // Calculating character facing angle
            float targetAngle = Mathf.Atan2(direction.x, direction.z) * Mathf.Rad2Deg + camTransform.eulerAngles.y;
            // Smoothing rotation
            float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetAngle, ref turnSmoothVelocity, turnSmoothTime);
            transform.rotation = Quaternion.Euler(0f, angle, 0f);

            // Applying movement
            Vector3 moveDirection = Quaternion.Euler(0f, targetAngle, 0f) * Vector3.forward;
            controller.Move(direction * speed * Time.deltaTime);
        }
    }
}
